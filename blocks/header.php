<header>
  <div id="banner_top" style="display:<?php echo select_banner_onoff($conn)?>"> <p><?php echo select_text_banner($conn) ?></p> </div>
  <nav>
    <a href="index.php" class="logo_a"><img class="logo" src="style/img/logo.png" alt="logo"></a>
    <div class="click">
      <a href="index.php">DOMŮ</a>
      <a href="partneri.php">PARTNEŘI</a>
      <a href="sluzby.php">SLUŽBY</a>
      <a href="kontakt.php">KONTAKTY</a>
      <a href="tel:<?php echo select_phone($conn) ?>"><i class="fas fa-phone fa-flip-horizontal"></i> <?php echo select_phone($conn) ?> </a>
    </div>
  </nav>
  <div class="swiper-container header_sw">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        <div class="swiper-slide swiper1 prvni"></div>
        <div class="swiper-slide swiper1 druhy"></div>
        <div class="swiper-slide swiper1 treti"></div>
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

</div>
</header>
