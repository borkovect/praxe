<section class="contact">

  <form name="form1" class="contact_form" action="" method="post" onsubmit="return validateForm()">
      <h2>POTŘEBUJETE OPRAVU ČI SERVIS?</h2><br />
      <span class="span_name"><p>JMÉNO A PŘÍJMENÍ</p><br /><input id="input_name" type="text" name="name" placeholder="Např. Jan Novák" onchange="check()"></label></span>

      <span class="span_phone"><p>TELEFONNÍ ČÍSLO</p><br /><input id="input_phone" type="tel" name="phone" placeholder="+420 123 456 789"></span><br />

      <span class="span_textarea"><p>POPIS ZÁVADY</p><br /><textarea id="input_textarea" name="content" placeholder="Zde prosím popište závadu nebo vzneste jakýkoliv dotaz..."></textarea></span><br />

      <span class="span_gdpr"> <p id="posledni_gdpr">Odesláním formuláře souhlasíte se zpracováním osobních údajů. více infomrací naleznete <a href="gdpr.php">zde</a> </p> </span><br />

      <input id="submit" type="submit" name="submit" value="ODESLAT">
  </form>
  <script type="text/javascript">
        function validateForm() {
          var x = document.forms["form1"]["name"].value;
          var y = document.forms["form1"]["phone"].value;
          var z = document.forms["form1"]["content"].value;
          if (x == "") {
          alert("Musíte vyplnit jméno");
          return false;
          }if (y == "") {
          alert("Musíte vyplnit telefonní číslo");
          return false;
          }if (z == "") {
          alert("Musíte napsat nějaký text");
          return false;
          }
      }
  </script>
</section>
