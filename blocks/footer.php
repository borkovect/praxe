<footer>

  <div class="content_footer">
      <div class="text_footer">
        <h3>JSME TADY PRO VÁS</h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vitae arcu tempor neque lacinia pretium. In convallis. Nullam dapibus fermentum ipsum. Etiam bibendum elit eget erat.</p>
      </div>
      <div class="kontakt_footer">
        <h3>NAPIŠTE ČI ZAVOLEJTE</h3>
        <p><a href="tel:<?php echo select_phone($conn) ?>"><i class="fas fa-phone fa-flip-horizontal"></i><?php echo select_phone($conn) ?></a><br></p>
        <p><a href="mailto:<?php echo select_email($conn) ?>"><i class="fas fa-envelope"></i><?php echo select_email($conn) ?></a></p>
      </div>
      <div class="map_footer">

      </div>
  </div>
</footer>
<div class="paticka_firma">
  <div class="content_paticka">
    <p>© Plynomontáž.cz <?php echo date("Y"); ?>, Provozuje Josef Kadlec</p>
    <p>Vytvořil <a target="_blank" href="https://kadlec-software.cz/">Kadlec-Software</a></p>
  </div>
</div>
