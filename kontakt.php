<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <?php include_once("sql/connect.php"); // Připojení databáze ?>
    <?php include_once("sql/function.php"); // Připojení funkcí ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plynomontáže Kadlec</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="style/style-media.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="style/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="style/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="style/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="style/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="style/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
  </head>
  <body>
      <?php
        include_once("blocks/header.php"); //Připojení hlavičky ?>

        <div class="kontakt_container">
          <div id="info">
            <h2>KONTAKTUJTE NÁS!</h2>
            <p><a href="tel:<?php echo select_phone($conn) ?>"><i class="fas fa-phone fa-flip-horizontal"></i><?php echo select_phone($conn) ?></a></p>
            <p><a href="mailto:<?php echo select_email($conn) ?>"><i class="fas fa-envelope"></i><?php echo select_email($conn) ?></a></p>
            <p>Naše adresa: <?php echo select_adresa($conn) ?>  <br>
              Doba pro přijímání hovorů:</p>
            <ul>
              <li>Pondělí: 8:30 – 18:30 </li>
              <li>Úterý:   8:30 – 18:30 </li>
              <li>Středa:  8:30 – 18:30 </li>
              <li>Čvrtek:  8:30 – 18:30 </li>
              <li>Pátek:   8:30 – 18:30 </li>
              <li>Sobota:  nepřijímáme </li>
              <li>Neděle:  nepřijímáme </li>
            </ul>
          </div>
          <div id="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d941.061081473113!2d13.34458259837677!3d49.74513033522709!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470af208f27eb489%3A0xe91d33710343fd2a!2zTWFsZXNpY2vDoSA3MDkvNSwgMzE4IDAwIFBsemXFiCAzLVNrdnLFiGFueQ!5e1!3m2!1scs!2scz!4v1559034938953!5m2!1scs!2scz" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>


      <?php
        include_once("blocks/contact.php"); //Připojení kontakt

        include_once("blocks/footer.php"); //Připojení patičky
      ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script>
      var swiper1 = new Swiper('.header_sw', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },

        autoplay: {
          delay: 4000,
        },
      })
    </script>
    <script>
      var swiper2 = new Swiper('.logo_sw', {
        slidesPerView: 4,
        spaceBetween: 30,
        loop: true,
        autoplay: {
          delay: 3000,
        },
      });
    </script>
  </body>
</html>
