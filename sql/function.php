<?php

// -------------------------------------------------- SELECT DATA FROM DB ---------------------------------------------

  function select_phone($conn)
  {
    mysqli_query($conn, "SET CHARACTER SET UTF8");
    $sql = "SELECT TelCislo FROM administrace";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            $TelCislo = $row["TelCislo"];
        }
    } else {
        $TelCislo = "Nepodařilo se dostat z databáze!";
      }

      return $TelCislo;

  }



  function select_email($conn)
  {
    mysqli_query($conn, "SET CHARACTER SET UTF8");
    $sql = "SELECT Email FROM administrace";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            $Email = $row["Email"];
        }
    } else {
        $Email = "Nepodařilo se dostat z databáze!";
      }

      return $Email;

  }



  function select_adresa($conn)
  {
    mysqli_query($conn, "SET CHARACTER SET UTF8");
    $sql = "SELECT Adresa FROM administrace";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            $Adresa = $row["Adresa"];
        }
    } else {
        $Adresa = "Nepodařilo se dostat z databáze!";
      }

      return $Adresa;

  }



    function select_about($conn)
    {
      mysqli_query($conn, "SET CHARACTER SET UTF8");
      $sql = "SELECT About FROM administrace";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          // output data of each row
          while($row = mysqli_fetch_assoc($result)) {
              $About = $row["About"];
          }
      } else {
          $About = "Nepodařilo se dostat z databáze!";
        }

        return $About;

    }



    function select_gdpr($conn)
    {
      mysqli_query($conn, "SET CHARACTER SET UTF8");
      $sql = "SELECT GDPR FROM administrace";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          // output data of each row
          while($row = mysqli_fetch_assoc($result)) {
              $GDPR = $row["GDPR"];
          }
      } else {
          $GDPR = "Nepodařilo se dostat z databáze!";
        }

        return $GDPR;

    }



    function select_text_banner($conn)
    {
      mysqli_query($conn, "SET CHARACTER SET UTF8");
      $sql = "SELECT BannerText FROM administrace";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          // output data of each row
          while($row = mysqli_fetch_assoc($result)) {
              $BannerText = $row["BannerText"];
          }
      } else {
          $BannerText = "Nepodařilo se dostat z databáze!";
        }

        return $BannerText;

    }




    function select_sponzor($conn)
    {

      $sql = "SELECT JmenoPartnera, JmenoSouboru, WebStranky FROM sponzor";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          while($row = mysqli_fetch_assoc($result)) {
              $JmenoPartnera = $row["JmenoPartnera"];
              $JmenoSouboru = $row["JmenoSouboru"];
              $WebStranky = "'" . $row["WebStranky"] . "'";

              echo '<div class="swiper-slide"> <img src="style/img/sponzor/' . $JmenoSouboru .'" alt="'. $JmenoPartnera .'" onclick="window.location.href='. $WebStranky .'"> </div>';
          }
      } else {

        }


    }



    function select_banner_onoff($conn)
    {
      mysqli_query($conn, "SET CHARACTER SET UTF8");
      $sql = "SELECT Banner FROM administrace";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          // output data of each row
          while($row = mysqli_fetch_assoc($result)) {
              $Banner = $row["Banner"];
          }
      } else {
          $Banner = "Nepodařilo se dostat z databáze!";
        }

      if ($Banner == 0){
        $display = "none";
      } else {
          $display = "block";
      }

      return $display;

    }







// -------------------------------------------------------- CHECKED FORM ADMIN --------------------------------------------

    function select_banner_onoff_radio($conn)
    {
      mysqli_query($conn, "SET CHARACTER SET UTF8");
      $sql = "SELECT Banner FROM administrace";
      $result = mysqli_query($conn, $sql);

      if (mysqli_num_rows($result) > 0) {
          // output data of each row
          while($row = mysqli_fetch_assoc($result)) {
              $Banner = $row["Banner"];
          }
      } else {
          $Banner = "Nepodařilo se dostat z databáze!";
        }

      if ($Banner == 0){
        $check = "";
      } else {
          $check = "checked";
      }

      return $check;

    }


// --------------------------------------------------- LOGIN ------------------------------------------------------------


    function login($conn, $username, $password){
      mysqli_query($conn, "SET CHARACTER SET UTF8");
      $sql = "SELECT jmeno, heslo FROM login";
      $result = mysqli_query($conn, $sql);

      while($row = mysqli_fetch_assoc($result)) {
          $db_jmeno = $row["jmeno"];
          $db_heslo = $row["heslo"];

          if ($db_jmeno == $username && $db_heslo == sha1($password)){
            echo header("Location: home.php");
          }

      }

      echo "Špatné uživatelské jméno nebo heslo";

  /*    if ($db_jmeno == $username && $db_heslo == sha1($password)){
        echo header("Location: home.php");
      } else {echo "Špatné uživatelské jméno nebo heslo";}  */

    }


//----------------------------------------------------- UPDATE -----------------------------------------------------------



    function update_phone($phone, $conn){

      $sql = "UPDATE administrace SET TelCislo='$phone' WHERE id=1";
      mysqli_query($conn, "SET CHARACTER SET UTF8");

      if (mysqli_query($conn, $sql)) {

      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }


    }



    function update_email($email, $conn){

      $sql = "UPDATE administrace SET Email='$email' WHERE id=1";
      mysqli_query($conn, "SET CHARACTER SET UTF8");

      if (mysqli_query($conn, $sql)) {

      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }


    }


    function update_adresa($adresa, $conn){

      $sql = "UPDATE administrace SET Adresa='$adresa' WHERE id=1";
      mysqli_query($conn, "SET CHARACTER SET UTF8");

      if (mysqli_query($conn, $sql)) {

      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }


    }


    function update_gdpr_text($gdpr_text, $conn){

      $sql = "UPDATE administrace SET GDPR='$gdpr_text' WHERE id=1";
      mysqli_query($conn, "SET CHARACTER SET UTF8");

      if (mysqli_query($conn, $sql)) {

      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }


    }



    function update_about_text($about_text, $conn){

      $sql = "UPDATE administrace SET About='$about_text' WHERE id=1";
      mysqli_query($conn, "SET CHARACTER SET UTF8");

      if (mysqli_query($conn, $sql)) {

      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }


    }



    function update_banner_check($banner_check, $conn){

      $sql = "UPDATE administrace SET Banner='$banner_check' WHERE id=1";
      mysqli_query($conn, "SET CHARACTER SET UTF8");

      if (mysqli_query($conn, $sql)) {

      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }


    }


    function update_banner_text($banner_text, $conn){

      $sql = "UPDATE administrace SET BannerText='$banner_text' WHERE id=1";
      mysqli_query($conn, "SET CHARACTER SET UTF8");

      if (mysqli_query($conn, $sql)) {

      } else {
          echo "Error updating record: " . mysqli_error($conn);
      }


    }

// --------------------------------------------------------------- ODSTRANENI UZIVATELE ----------------------------------------------------------------


    function delete_login($conn, $id)
    {


      $sql = "DELETE FROM login WHERE id=$id";

    if (mysqli_query($conn, $sql)) {

    } else {
        echo "Error deleting record: " . mysqli_error($conn);
    }


  }

// --------------------------------------------------------------- PRIDANI UZIVATELE ----------------------------------------------------------------

  function add_login($conn, $user, $password)
  {

    $passwordsha1 = sha1($password);

    $sql = "INSERT INTO login (jmeno, heslo) VALUES ('$user', '$passwordsha1')";

    if (mysqli_query($conn, $sql)) {

    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }


}


 ?>
