<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <?php include_once("../sql/connect.php"); // Připojení databáze ?>
    <?php include_once("../sql/function.php"); // Připojení funkcí ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plynomontáže Kadlec</title>
    <link rel="stylesheet" href="styl_admin.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="../style/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../style/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../style/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../style/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="../style/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
  </head>
  <body>

    <div class="update_box">
      <a href="../index.php"><img src="logo_circle.png" alt="logo"></a>
      <div class="update_provedeny">
        <h2><?php



        if (isset($_POST['submit'])){


          $target_dir = "../style/img/sponzor/";
          $target_file = $target_dir . basename($_FILES["file_sponzor"]["name"]);
          $uploadOk = 1;
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Check if image file is a actual image or fake image
          if(isset($_POST["submit"])) {
              $check = getimagesize($_FILES["file_sponzor"]["tmp_name"]);
              if($check !== false and $check[0] == 264 and $check[1] == 199) {

                  $uploadOk = 1;
              } else {
                  echo "Soubor není obrázek<br>";
                  $uploadOk = 0;
                  exit;
              }
          }
          // Check if file already exists
          if (file_exists($target_file)) {
              echo "Tuto logo již existuje<br>";
              $uploadOk = 0;
              exit;
          }
          // Check file size
          if ($_FILES["file_sponzor"]["size"] > 500000) {
              echo "Fotka má moc velkou velikost<br>";
              $uploadOk = 0;
              exit;
          }
          // Allow certain file formats
          if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
          && $imageFileType != "gif" ) {
              echo "Jsou povoleny jenom JPG, JPEG, PNG & GIF soubory<br>";
              $uploadOk = 0;
              exit;
          }
          // Check if $uploadOk is set to 0 by an error
          if ($uploadOk == 0) {
              echo "Logo nebylo nahráno<br>";
              exit;
          // if everything is ok, try to upload file
          } else {
              if (move_uploaded_file($_FILES["file_sponzor"]["tmp_name"], $target_file)) {
                  echo "Všechno proběhlo v pořádku<br>";
              } else {
                  echo "Logo nebylo nahráno<br>";
                  exit;
              }
          }

          $name_partner = $_POST['name_sponzor'];
          $destination_sponzor = basename($_FILES["file_sponzor"]["name"]);
          $url_sponzor = $_POST['url_sponzor'];

          $sql = "INSERT INTO sponzor (JmenoPartnera, JmenoSouboru, WebStranky) VALUES ('$name_partner', '$destination_sponzor', '$url_sponzor')";

          if (mysqli_query($conn, $sql)) {
                
          } else {
              echo "Error: " . $sql . "<br>" . mysqli_error($conn);
          }

        }
          ?></h2>
        <p> <a href="home.php"> Zpět na administraci </a></p>
        <p> <a href="../index.php"> Zpět na hlavní stránku </a></p>
      </div>
    </div>




  </body>
</html>
