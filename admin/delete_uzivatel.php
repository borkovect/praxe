<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <?php include_once("../sql/connect.php"); // Připojení databáze ?>
    <?php include_once("../sql/function.php"); // Připojení funkcí ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plynomontáže Kadlec</title>
    <link rel="stylesheet" href="styl_admin.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="../style/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../style/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../style/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../style/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="../style/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
  </head>
  <body>

    <div class="update_box">
      <a href="../index.php"><img src="logo_circle.png" alt="logo"></a>
      <div class="update_provedeny">
        <h2>UŽIVATEL ÚSPĚŠNĚ VYMAZÁN</h2>
        <p> <a href="home.php"> Zpět na administraci </a></p>
        <p> <a href="../index.php"> Zpět na hlavní stránku </a></p>
      </div>
    </div>

    <?php

    if (isset($_POST['delete'])) {
        $delete = $_POST['delete'];
    }

    foreach ($delete as $id) {
      delete_login($conn, $id);
    }

     ?>

  </body>
</html>
