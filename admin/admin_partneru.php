<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <?php include_once("../sql/connect.php"); // Připojení databáze ?>
    <?php include_once("../sql/function.php"); // Připojení funkcí ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrace stránky</title>
    <link rel="stylesheet" href="styl_admin.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="../style/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../style/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../style/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../style/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="../style/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
  </head>
  <body>

    <div class="admin_box">
      <a href="../index.php"><img src="logo_circle.png" alt="logo"></a>
      <form class="admin_form" action="sponzor_pridan.php" method="post" enctype="multipart/form-data">
        <h2>ADMINISTRACE PARTNERŮ</h2>
          <p> <a href="admin_uzivatelu.php"> Administrace uživatelů </a></p>
          <p> <a href="home.php"> Administrace stránky </a></p>
          <p>Jméno partnera</p><br>
          <input type="text" name="name_sponzor"><br>
          <p>Odkaz na webové stránky</p><br>
          <input type="text" name="url_sponzor"><br>
          <p>Logo (dovolí nahrát jenom logo ve velikosti 264x199px)</p><br>
          <input type="file" name="file_sponzor"><br>
          <input type="submit" name="submit" value="Odeslat"><br>

      </form>
    </div>

  </body>
</html>
