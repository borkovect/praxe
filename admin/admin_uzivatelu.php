<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <?php include_once("../sql/connect.php"); // Připojení databáze ?>
    <?php include_once("../sql/function.php"); // Připojení funkcí ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrace stránky</title>
    <link rel="stylesheet" href="styl_admin.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="../style/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../style/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../style/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../style/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="../style/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
  </head>
  <body>

    <div class="admin_box">
      <a href="../index.php"><img src="logo_circle.png" alt="logo"></a>
      <form class="admin_form" action="delete_uzivatel.php" method="post">
        <h2>ADMINISTRACE UŽIVATELŮ</h2>
          <p> <a href="home.php"> Administrace stránky </a></p>
          <p> <a href="admin_partneru.php"> Administrace partnerů </a></p>
          <h3>Vymazat uživatele<br>
          (zaškrtnutí uživatelé se odstraní!)</h3><br>

          <?php


          mysqli_query($conn, "SET CHARACTER SET UTF8");
          $sql = "SELECT id, jmeno, heslo FROM login";
          $result = mysqli_query($conn, $sql);

          while($row = mysqli_fetch_assoc($result)) {

              $login_id = $row["id"];
              $login_jmeno = $row["jmeno"];
              $login_heslo = $row["heslo"];

          echo '<p>'. $login_jmeno ;
          echo '<input id="check_delete" type="checkbox" name="delete[]" value="'. $login_id .'"></p><br>';


        }

           ?>

           <input type="submit" name="submit" value="Odebrat"><br>

      </form>

      <form class="add_user" class="admin_form" action="add_uzivatel.php" method="post">
          <h3>Přidat uživatele</h3>
          <p>Jméno</p><br>
          <input type="text" name="user_add"><br>
          <p>Heslo</p><br>
          <input type="password" name="password_add"><br>
          <input type="submit" name="submit2" value="Přidat"><br>

      </form>
    </div>

  </body>
</html>
