<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <?php include_once("sql/connect.php"); // Připojení databáze ?>
    <?php include_once("sql/function.php"); // Připojení funkcí ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plynomontáže Kadlec</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="style/style-media.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="style/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="style/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="style/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="style/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="style/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
  </head>
  <body>
      <?php
        include_once("blocks/header.php"); //Připojení hlavičky?>

        <div class="services_details">

          <div class="sluzba_div">
            <div class="kolecko"><i class="fas fa-tools"></i></div>
            <div>
              <h4>SERVIS 1</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer tempor. In dapibus augue non sapien. Nulla non lectus sed nisl molestie malesuada. Nulla quis diam. Sed convallis magna eu sem. Nam sed tellus id magna elementum tincidunt.</p>
            </div>
          </div>

          <div class="sluzba_div">
            <div class="kolecko kolecko_right"><i class="fas fa-handshake"></i></div>
            <div>
              <h4>SERVIS 2</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer tempor. In dapibus augue non sapien. Nulla non lectus sed nisl molestie malesuada. Nulla quis diam. Sed convallis magna eu sem. Nam sed tellus id magna elementum tincidunt.</p>
            </div>
          </div>

          <div class="sluzba_div">
            <div class="kolecko"><i class="fas fa-business-time"></i></div>
            <div>
              <h4>SERVIS 3</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer tempor. In dapibus augue non sapien. Nulla non lectus sed nisl molestie malesuada. Nulla quis diam. Sed convallis magna eu sem. Nam sed tellus id magna elementum tincidunt.</p>
            </div>
          </div>

          <div class="sluzba_div">
            <div class="kolecko kolecko_right"><i class="fas fa-money-bill-wave"></i></div>
            <div>
              <h4>SERVIS 4</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer tempor. In dapibus augue non sapien. Nulla non lectus sed nisl molestie malesuada. Nulla quis diam. Sed convallis magna eu sem. Nam sed tellus id magna elementum tincidunt.</p>
            </div>
          </div>




        </div>


      <?php
        include_once("blocks/contact.php"); //Připojení kontakt

        include_once("blocks/footer.php"); //Připojení patičky
      ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script>
      var swiper1 = new Swiper('.header_sw', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },

        autoplay: {
          delay: 4000,
        },
      })
    </script>
    <script>
      var swiper2 = new Swiper('.logo_sw', {
        slidesPerView: 4,
        spaceBetween: 30,
        loop: true,
        autoplay: {
          delay: 3000,
        },
      });
    </script>
  </body>
</html>
